import { v4 as uuid } from 'uuid';

export class DestinoViaje {
    
    private selected: boolean;
    servicios: string[];
    id = uuid();

    constructor(public nombre: string, public u: string, public votes: number = 0) {
        this.servicios = ['piscina', 'desayuno'];
    }
    isSelected() {
        return this.selected;
    }
    setSelected(s: boolean) {
        this.selected = s;
    }
    voteUp() {
        this.votes++;
    }
    voteDown() {
        this.votes--;
    }
}
